const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./credentials.json');

    class List {
        static async getUserToken(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password,
                })
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => (res.token_type + ' ' + res.access_token))
            
        };

        static async sendCreateListRequest(url, token, args) {
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify({
                   name: args.listNameForDelete
                })
            });
        };
    };

    async function createList(url, args) {

        console.log(`Creating list for ${url}`);

        const userToken = await List.getUserToken(url, args);
       
        const response = await List.sendCreateListRequest(url, userToken, args);

        if (response.status === 201) {
            console.log(`List is created on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating List=========');
        createList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
