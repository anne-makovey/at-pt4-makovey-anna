const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./credentials.json');

    class Client {
        static async getUserToken(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.emailForDelete,
                    password: args.passwordForDelete,
                })
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => (res.token_type + ' ' + res.access_token))
        };

        static async getUserId(url, token) {
            return fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => res.id)
        };

        static async sendDeleteUserRequest(url, token, id) {
            return fetch(`${url}/users/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            });
        };

    };

    async function deleteUser(url, args) {
        console.log(`Deleting user for ${url}`);

        const userToken = await Client.getUserToken(url, args);
        console.log(userToken);
        const userId = await Client.getUserId(url, userToken);
        const response = await Client.sendDeleteUserRequest(url, userToken, userId);

        if (response.status === 200) {
            console.log(`User is deleted on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully delete user for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting User=========');
        deleteUser(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
