const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./credentials.json');

    class List {
        static async getUserToken(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password,
                })
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => (res.token_type + ' ' + res.access_token))
        };

        static async getUserId(url, token) {
            return fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => res.id)
        };

        static async getListId(url, token, userId, args) {
            return fetch(`${url}/users/${userId}/lists`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token,
                },
            })
            .then(res => res.json())
            .then(res => res.data)
            .then(res => {
                for (let i = 0; i < 9; i++) {
                    if (res[i].name === args.listNameForDelete) {
                        return res[i].id;
                    }
                }
            })
    };

        static async sendDeleteListRequest(url, token, listId) {
            return fetch(`${url}/user-lists/${listId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            });
        };
    };

    async function deleteList(url, args) {

        console.log(`Deleting list for ${url}`);

        const userToken = await List.getUserToken(url, args);
        console.log(userToken);
        const userId = await List.getUserId(url, userToken);
        const listId = await List.getListId(url, userToken, userId, args);
        const response = await List.sendDeleteListRequest(url, userToken, listId);

        if (response.status === 200) {
            console.log(`List is deleted on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully delete list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting List=========');
        deleteList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
