const LoginActions = require('../specs/Login/actions/Login_pa');
const page = new LoginActions();
const randomEmail = require('random-email');
const credentials = require('./../credentials.json');

class HelpClass 
{

    clickItemInList(name) {
        const place = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]`);
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        place[0].scrollIntoView({block: "center"});
        place[0].click();
    }

    browserClick(elm){
        return browser.execute((e) => {
            document.querySelectorAll(e).click();
        }, elm);
    }

    browserClickOnArrayElement(elm, index){
        return browser.execute((e, i) => {
            document.querySelectorAll(e)[i - 1].click();
        }, elm, index);
    }

    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.clickLoginButton();
    }
    
    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(email);
        page.enterPassword(password);
        page.clickLoginButton();
    }

    createRandomEmail() {
        return randomEmail({domain: 'example.com'});
    }

    editItemInList(name) {
        const editList = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]/../../..//a[contains(., "Edit")]`);
        if (editList.length === 0) {
            throw new Error("Element not found");
        }
        editList[0].scrollIntoView({block: "center"});
        editList[0].click();
    }

}

module.exports = new HelpClass();