const UserRegistrationActions = require('./actions/UserRegistration_pa')
const MenuActions = require('../Menu/actions/menu_pa');
const credentials = require('./../../credentials.json');
const validate = require('../../helpers/validators');


const menuSteps = new MenuActions();
const pageSteps = new UserRegistrationActions();


describe('User registration on the Hedonist site', () => {
    
    beforeEach(() => {
        browser.url(credentials.appUrl);
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should register with valid data', () => {
        menuSteps.navigateToSignUp();

        pageSteps.enterFirstName(credentials.firstNameForDelete);
        pageSteps.enterLastName(credentials.lastNameForDelete);
        pageSteps.enterEmail(credentials.emailForDelete);
        pageSteps.enterPassword(credentials.passwordForDelete);
        pageSteps.clickCreateButton();
        
        validate.successNotificationTextIs(credentials.registrationSuccessText);
        validate.pageTransition('login');
    });

}); 
